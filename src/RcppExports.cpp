// Generated by using Rcpp::compileAttributes() -> do not edit by hand
// Generator token: 10BE3573-1514-4C36-9D1C-5A225CD40393

#include <Rcpp.h>

using namespace Rcpp;

// char_set_ratio
double char_set_ratio(const std::string& x_s, const std::string& y_s, const SEXP& method, const SEXP& weight, const SEXP& p, const SEXP& bt, const SEXP& q, const SEXP& use_bytes, const SEXP& n_thread);
RcppExport SEXP _matchr_char_set_ratio(SEXP x_sSEXP, SEXP y_sSEXP, SEXP methodSEXP, SEXP weightSEXP, SEXP pSEXP, SEXP btSEXP, SEXP qSEXP, SEXP use_bytesSEXP, SEXP n_threadSEXP) {
BEGIN_RCPP
    Rcpp::RObject rcpp_result_gen;
    Rcpp::RNGScope rcpp_rngScope_gen;
    Rcpp::traits::input_parameter< const std::string& >::type x_s(x_sSEXP);
    Rcpp::traits::input_parameter< const std::string& >::type y_s(y_sSEXP);
    Rcpp::traits::input_parameter< const SEXP& >::type method(methodSEXP);
    Rcpp::traits::input_parameter< const SEXP& >::type weight(weightSEXP);
    Rcpp::traits::input_parameter< const SEXP& >::type p(pSEXP);
    Rcpp::traits::input_parameter< const SEXP& >::type bt(btSEXP);
    Rcpp::traits::input_parameter< const SEXP& >::type q(qSEXP);
    Rcpp::traits::input_parameter< const SEXP& >::type use_bytes(use_bytesSEXP);
    Rcpp::traits::input_parameter< const SEXP& >::type n_thread(n_threadSEXP);
    rcpp_result_gen = Rcpp::wrap(char_set_ratio(x_s, y_s, method, weight, p, bt, q, use_bytes, n_thread));
    return rcpp_result_gen;
END_RCPP
}
// token_set_ratio
double token_set_ratio(const std::string& x_s, const std::string& y_s, const SEXP& method, const SEXP& weight, const SEXP& p, const SEXP& bt, const SEXP& q, const SEXP& use_bytes, const SEXP& n_thread);
RcppExport SEXP _matchr_token_set_ratio(SEXP x_sSEXP, SEXP y_sSEXP, SEXP methodSEXP, SEXP weightSEXP, SEXP pSEXP, SEXP btSEXP, SEXP qSEXP, SEXP use_bytesSEXP, SEXP n_threadSEXP) {
BEGIN_RCPP
    Rcpp::RObject rcpp_result_gen;
    Rcpp::RNGScope rcpp_rngScope_gen;
    Rcpp::traits::input_parameter< const std::string& >::type x_s(x_sSEXP);
    Rcpp::traits::input_parameter< const std::string& >::type y_s(y_sSEXP);
    Rcpp::traits::input_parameter< const SEXP& >::type method(methodSEXP);
    Rcpp::traits::input_parameter< const SEXP& >::type weight(weightSEXP);
    Rcpp::traits::input_parameter< const SEXP& >::type p(pSEXP);
    Rcpp::traits::input_parameter< const SEXP& >::type bt(btSEXP);
    Rcpp::traits::input_parameter< const SEXP& >::type q(qSEXP);
    Rcpp::traits::input_parameter< const SEXP& >::type use_bytes(use_bytesSEXP);
    Rcpp::traits::input_parameter< const SEXP& >::type n_thread(n_threadSEXP);
    rcpp_result_gen = Rcpp::wrap(token_set_ratio(x_s, y_s, method, weight, p, bt, q, use_bytes, n_thread));
    return rcpp_result_gen;
END_RCPP
}
// token_set_ratio_list
std::vector<double> token_set_ratio_list(CharacterVector& x, CharacterVector& y, const SEXP& method, const SEXP& weight, const SEXP& p, const SEXP& bt, const SEXP& q, const SEXP& use_bytes, const SEXP& n_thread);
RcppExport SEXP _matchr_token_set_ratio_list(SEXP xSEXP, SEXP ySEXP, SEXP methodSEXP, SEXP weightSEXP, SEXP pSEXP, SEXP btSEXP, SEXP qSEXP, SEXP use_bytesSEXP, SEXP n_threadSEXP) {
BEGIN_RCPP
    Rcpp::RObject rcpp_result_gen;
    Rcpp::RNGScope rcpp_rngScope_gen;
    Rcpp::traits::input_parameter< CharacterVector& >::type x(xSEXP);
    Rcpp::traits::input_parameter< CharacterVector& >::type y(ySEXP);
    Rcpp::traits::input_parameter< const SEXP& >::type method(methodSEXP);
    Rcpp::traits::input_parameter< const SEXP& >::type weight(weightSEXP);
    Rcpp::traits::input_parameter< const SEXP& >::type p(pSEXP);
    Rcpp::traits::input_parameter< const SEXP& >::type bt(btSEXP);
    Rcpp::traits::input_parameter< const SEXP& >::type q(qSEXP);
    Rcpp::traits::input_parameter< const SEXP& >::type use_bytes(use_bytesSEXP);
    Rcpp::traits::input_parameter< const SEXP& >::type n_thread(n_threadSEXP);
    rcpp_result_gen = Rcpp::wrap(token_set_ratio_list(x, y, method, weight, p, bt, q, use_bytes, n_thread));
    return rcpp_result_gen;
END_RCPP
}

static const R_CallMethodDef CallEntries[] = {
    {"_matchr_char_set_ratio", (DL_FUNC) &_matchr_char_set_ratio, 9},
    {"_matchr_token_set_ratio", (DL_FUNC) &_matchr_token_set_ratio, 9},
    {"_matchr_token_set_ratio_list", (DL_FUNC) &_matchr_token_set_ratio_list, 9},
    {NULL, NULL, 0}
};

RcppExport void R_init_matchr(DllInfo *dll) {
    R_registerRoutines(dll, NULL, CallEntries, NULL, NULL);
    R_useDynamicSymbols(dll, FALSE);
}
