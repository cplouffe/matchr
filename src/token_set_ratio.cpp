#include <Rcpp.h>
#include <R_ext/Rdynload.h>
#include <Rdefines.h>
#include <stringdist_api.h>
#include <infix_iterator.h>
#include <set>
#include <iostream>
#include <sstream>
#include <string>
#include <thread>
using namespace Rcpp;

std::string trim(const std::string& str) {
  size_t first = str.find_first_not_of(' ');
  if (std::string::npos == first) {
    return str;
  }
  size_t last = str.find_last_not_of(' ');
  return str.substr(first, (last - first + 1));
}

// Get greatest element
bool comp(int a, int b) {
  return (a < b);
}

double normalize_distance(std::string &x, std::string &y, double &dist) {
  double max_dist = x.length() + y.length();
  if (max_dist == 0.0) {
    max_dist = 1.0;
  }
  double similarity = 1 - dist / max_dist;
  return(similarity);
}

// Force min length function
double force_min_length(std::string &x, std::string &y, double offset) {
  double min_length = (x.length() - y.length() + offset) > 0.0 ? 1.0 : 0.0;
  return(min_length);
}

// [[Rcpp::export]]
double char_set_ratio(const std::string &x_s,
                      const std::string &y_s,
                      const SEXP &method,
                      const SEXP &weight,
                      const SEXP &p,
                      const SEXP &bt,
                      const SEXP &q,
                      const SEXP &use_bytes,
                      const SEXP &n_thread) {

  // // unsigned int concurrent_threads = std::thread::hardware_concurrency();
  std::set<char> s1(x_s.begin(), x_s.end());
  std::set<char> s2(y_s.begin(), y_s.end());

  std::set<char> intersect;
  std::set<char> diff_s1_to_s2;
  std::set<char> diff_s2_to_s1;

  set_intersection(s1.begin(), s1.end(), s2.begin(), s2.end(),
                   std::inserter(intersect, intersect.begin()));

  set_difference(s1.begin(), s1.end(), s2.begin(), s2.end(),
                 std::inserter(diff_s1_to_s2, diff_s1_to_s2.begin()));

  set_difference(s2.begin(), s2.end(), s1.begin(), s1.end(),
                 std::inserter(diff_s2_to_s1, diff_s2_to_s1.begin()));

  std::stringstream sorted_sect_stream;
  std::stringstream comb_s1_to_s2_stream;
  std::stringstream comb_s2_to_s1_stream;

  std::copy(intersect.begin(), intersect.end(),
            infix_ostream_iterator<char>(sorted_sect_stream, " "));
  std::copy(diff_s1_to_s2.begin(), diff_s1_to_s2.end(),
            infix_ostream_iterator<char>(comb_s1_to_s2_stream, " "));
  std::copy(diff_s2_to_s1.begin(), diff_s2_to_s1.end(),
            infix_ostream_iterator<char>(comb_s2_to_s1_stream, " "));

  std::string sorted_sect_string(sorted_sect_stream.str());
  CharacterVector sorted_sect = CharacterVector::create(sorted_sect_string);

  std::string comb_s1_to_s2_string(trim(sorted_sect_string + " " + comb_s1_to_s2_stream.str()));
  CharacterVector comb_s1_to_s2 = CharacterVector::create(comb_s1_to_s2_string);

  std::string comb_s2_to_s1_string(trim(sorted_sect_string + " " + comb_s2_to_s1_stream.str()));
  CharacterVector comb_s2_to_s1 = CharacterVector::create(comb_s2_to_s1_string);

  NumericVector dist_1 = sd_stringdist(sorted_sect,
                                       comb_s1_to_s2,
                                       method,
                                       weight,
                                       p,
                                       bt,
                                       q,
                                       use_bytes,
                                       n_thread);

  NumericVector dist_2 = sd_stringdist(sorted_sect,
                                       comb_s2_to_s1,
                                       method,
                                       weight,
                                       p,
                                       bt,
                                       q,
                                       use_bytes,
                                       n_thread);

  NumericVector dist_3 = sd_stringdist(comb_s1_to_s2,
                                       comb_s2_to_s1,
                                       method,
                                       weight,
                                       p,
                                       bt,
                                       q,
                                       use_bytes,
                                       n_thread);

  // Compute n = n_a + n_b, insert a new vector of size n, insert a,
  // insert b -- and wrap all that in a little helper function.
  std::vector<double> pw(3);

  pw[0] = normalize_distance(sorted_sect_string,
                             comb_s1_to_s2_string,
                             dist_1[0]);
  pw[1] = normalize_distance(sorted_sect_string,
                             comb_s2_to_s1_string,
                             dist_2[0]);
  pw[2] = normalize_distance(comb_s1_to_s2_string,
                             comb_s2_to_s1_string,
                             dist_3[0]);

  std::vector<double>::iterator dist;

  dist = std::max_element(pw.begin(), pw.end());

  return *dist;

}

// [[Rcpp::export]]
double token_set_ratio(const std::string &x_s,
                       const std::string &y_s,
                       const SEXP &method,
                       const SEXP &weight,
                       const SEXP &p,
                       const SEXP &bt,
                       const SEXP &q,
                       const SEXP &use_bytes,
                       const SEXP &n_thread) {

  // // unsigned int concurrent_threads = std::thread::hardware_concurrency();
  std::istringstream x_stream(x_s);
  std::set<std::string> s1((std::istream_iterator<std::string>(x_stream)),
                           std::istream_iterator<std::string>());

  std::istringstream y_stream(y_s);
  std::set<std::string> s2((std::istream_iterator<std::string>(y_stream)),
                           std::istream_iterator<std::string>());

  std::ostringstream intersect;
  std::ostringstream diff_s1_to_s2;
  std::ostringstream diff_s2_to_s1;

  set_intersection(s1.begin(), s1.end(), s2.begin(), s2.end(),
                   std::ostream_iterator<std::string>(intersect, " "));
  set_difference(s1.begin(), s1.end(), s2.begin(), s2.end(),
                 std::ostream_iterator<std::string>(diff_s1_to_s2, " "));
  set_difference(s2.begin(), s2.end(), s1.begin(), s1.end(),
                 std::ostream_iterator<std::string>(diff_s2_to_s1, " "));

  std::string sorted_sect_string(intersect.str());
  CharacterVector sorted_sect = CharacterVector::create(sorted_sect_string);

  std::string comb_s1_to_s2_string(trim(sorted_sect_string + " " + diff_s1_to_s2.str()));
  CharacterVector comb_s1_to_s2 = CharacterVector::create(comb_s1_to_s2_string);

  std::string comb_s2_to_s1_string(trim(sorted_sect_string + " " + diff_s2_to_s1.str()));
  CharacterVector comb_s2_to_s1 = CharacterVector::create(comb_s2_to_s1_string);

  NumericVector dist_1 = sd_stringdist(sorted_sect,
                                       comb_s1_to_s2,
                                       method,
                                       weight,
                                       p,
                                       bt,
                                       q,
                                       use_bytes,
                                       n_thread);

  NumericVector dist_2 = sd_stringdist(sorted_sect,
                                       comb_s2_to_s1,
                                       method,
                                       weight,
                                       p,
                                       bt,
                                       q,
                                       use_bytes,
                                       n_thread);

  NumericVector dist_3 = sd_stringdist(comb_s1_to_s2,
                                       comb_s2_to_s1,
                                       method,
                                       weight,
                                       p,
                                       bt,
                                       q,
                                       use_bytes,
                                       n_thread);

  // Compute n = n_a + n_b, insert a new vector of size n, insert a,
  // insert b -- and wrap all that in a little helper function.
  std::vector<double> pw(3);

  pw[0] = normalize_distance(sorted_sect_string,
                             comb_s1_to_s2_string,
                             dist_1[0]);
  pw[1] = normalize_distance(sorted_sect_string,
                             comb_s2_to_s1_string,
                             dist_2[0]);
  pw[2] = normalize_distance(comb_s1_to_s2_string,
                             comb_s2_to_s1_string,
                             dist_3[0]);


  // std::cout << sorted_sect_string << std::endl;
  // std::cout << comb_s1_to_s2_string << std::endl;
  // std::cout << comb_s2_to_s1_string << std::endl;
  // std::cout << pw[0] << std::endl;
  // std::cout << dist_1[0] << std::endl;
  // std::cout << pw[1] << std::endl;
  // std::cout << dist_2[0] << std::endl;
  // std::cout << pw[2] << std::endl;
  // std::cout << dist_3[0] << std::endl;

  std::vector<double>::iterator dist;

  // Get the max of the distances
  dist = std::max_element(pw.begin(), pw.end());

  return *dist;

}

// [[Rcpp::export]]
std::vector<double> token_set_ratio_list(CharacterVector &x,
                                         CharacterVector &y,
                                         const SEXP &method,
                                         const SEXP &weight,
                                         const SEXP &p,
                                         const SEXP &bt,
                                         const SEXP &q,
                                         const SEXP &use_bytes,
                                         const SEXP &n_thread) {

  int n_x = x.size();
  int n_y = y.size();

  std::vector<double> result;
  std::vector<double> distances;

  for (int i = 0; i < n_x; i++) {
    for (int j = 0; j < n_y; j++) {
      distances.push_back(
        token_set_ratio(std::string(x[i]),
                        std::string(y[j]),
                        method,
                        weight,
                        p,
                        bt,
                        q,
                        use_bytes,
                        n_thread)
      );
    }
    result.push_back(*std::max_element(distances.begin(), distances.end()));
    distances.clear();
  }

  return result;

}
